---
title: "Sobre mi"
weight: 3
header_menu: true
---

![María y José](images/iglesia-chara.jpg)

##### Experiencia Profesional

Nuestros expertos han creado guías profesionales de compra para que tengas toda la información o servicio que buscas.

Resuelve tus dudas con nuestras guias. Obten los mejores presupuestos.

Ponemos nuestra experiencia, capcidad tecnológica y logística al servicio de sus clientes, asesorándolos y aportándoles valor en la gestión de la categoría en general y de su marca en particular.
Presencia internacional

La sede del grupo está en Charallave (Vlza)

Dispone de fábricas en Valencia (España). Tiene sedes en Madrid, Lisboa, París, Londres, Panama y Chile.
