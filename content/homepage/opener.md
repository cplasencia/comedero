---
title: "Bienvenidos"
weight: 1
---

Si eres amante de los sabores intensos, estás en el sitio adecuado. Estas son nuestras conservas picantes Frinsa, elaboradas de forma artesanal con ingredientes de primera calidad, y también las de nuestra marca portuguesa Minerva, que por tradición también incorpora una amplia gama de productos de este tipo perfectos para elaborar deliciosos aperitivos y recetas con un toque más exótico.

Todas se elaboran de manera artesanal, a mano, y con pescados siempre frescos de temporada en nuestra fábrica de A Poveira, mezclando siempre tradición con la mejor calidad. Descubre nuestras conservas picantes gourmet y dale una vuelta a tus aperitivos y recetas.
